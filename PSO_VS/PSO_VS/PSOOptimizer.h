#pragma once
#include <vector>
#include <functional>

class PSOOptimizer
{
public:
	void SetDefaultHyperParameters();
	void SetSearchDimensions(int iDimensionsOfSearchSpace);
	void SetObjectiveFunction(std::function<int(std::vector<double>&, double&)> iObjectiveFunction);
	void Optimize();

private:
	// Eventually this must all be replaced with vector operations - write your own matrix/vector class
	// Each particle must be defined properly with its own position and velocity etc.
	// For now naive implementation of PSO
	// Hyper parameters for PSO
	int mNumberOfParticles;
	int mNumberOfIterations;
	double mStepSize;
	double c1;
	double c2;
	double w;


	// Parameters for the objective function
	std::function<int(std::vector<double>&, double&)> mObjectiveFunction;
	int mDimensionsOfSearchSpace;
	std::vector<double> mUpperBounds;
	std::vector<double> mLowerBounds;
	double mBestFitness;

	// Dynamical variables 
	std::vector<std::vector<double>> mParticlePositions;
	std::vector<std::vector<double>> mParticleVelocities;
	std::vector<std::vector<double> > mPBest;
	std::vector<double> mGBest;

	std::vector<double> mPreviousParticleBestFitness;
	double mPreviousGBestFitness;

	// Private helper functions
	void Initialize();
	std::vector<double> EvaluateParticleFitnesses();
	void UpdateParticleAndGlobalBest(std::vector<double> iCurrentParticleFitnesses);
	void UpdatePositionsAndVelocities();

};