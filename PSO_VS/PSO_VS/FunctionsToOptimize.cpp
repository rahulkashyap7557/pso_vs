#include "FunctionsToOptimize.h"
#include <iostream>
#include <omp.h>


// This function will calculate Sum_i (x_i)^2 
int FunctionsToOptimize::func1(std::vector<double>& iInputArguments, double& oValToReturn)
{
	oValToReturn = 0.;
	int errCodeToReturn = 1;

	//if (iInputArguments.empty() == false)
	//{
		unsigned int inputSize = iInputArguments.size();		
		double retVal = 0.;
#pragma omp simd reduction(+: retVal)
		for (unsigned int ii = 0; ii < inputSize; ii++)
		{
			retVal += iInputArguments[ii] * iInputArguments[ii];
		}

		//// the objective function value must always be positive - print if it is not

		//if (oValToReturn < 0)
		//{
		//	std::cout << "Negative value of fitness!" << std::endl;
		//}
		oValToReturn = retVal;
		errCodeToReturn = 0;
	//}
	//else
	//{
	//	// Complain that the inputs are empty!!
	//	std::cout << "The function you are trying to optimize just received an empty vector!!!" << std::endl;
	//	errCodeToReturn = 1;		
	//}

	return errCodeToReturn;
}
