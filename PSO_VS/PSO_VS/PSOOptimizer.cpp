#include "PSOOptimizer.h"
#include <iostream>
#include <omp.h>
#include <algorithm>
#include <random>
#include <chrono>

bool FLIP_LOOPS = false;

// FAST BUT DOES NOT WORK WELL!! DON'T USE!!
int fastrand() 
{
	static unsigned int g_seed = (214013 * g_seed + 2531011);
	return (g_seed >> 16) & 0x7FFF;
}

void PSOOptimizer::SetDefaultHyperParameters()
{
	mNumberOfIterations = 100;
	mNumberOfParticles = 300;
	mStepSize = 0.1;
	c1 = 0.4;
	c2 = 0.4;
	w = 0.8;

	mUpperBounds = std::vector<double>{ 10., 10., 10., 10.};
	mLowerBounds = std::vector<double>{-10., -10., -10., -10.};
}

void PSOOptimizer::SetSearchDimensions(int iDimensionsOfSearchSpace)
{
	mDimensionsOfSearchSpace = iDimensionsOfSearchSpace;
}

void PSOOptimizer::SetObjectiveFunction(std::function<int(std::vector<double>&, double&)> iObjectiveFunction)
{
	mObjectiveFunction = iObjectiveFunction;
}

void PSOOptimizer::Optimize()
{
	// Set up default upper bounds and lower bounds if they have not been specified

	// TODO!! Set up default upper and lower bounds
	
	Initialize();	

	// Loop over the iterations and update the fitnesses for each of the particles
	for (int iteration = 0; iteration < mNumberOfIterations; iteration++)
	{
		
		// Evaluate fitness currently for each of the particles
		std::vector<double> currentParticleFitnesses = EvaluateParticleFitnesses();	


		// Update the global and particle best solutions and fitnesses
		UpdateParticleAndGlobalBest(currentParticleFitnesses);
		mPreviousParticleBestFitness = currentParticleFitnesses;

		// Update the positions and velocities until either tolerance is met or iterations are done
		UpdatePositionsAndVelocities();
	}

	
	// Show some output and exit
	std::cout << "The particle best position is: " << std::endl;
	for (int dimension = 0; dimension < mDimensionsOfSearchSpace; dimension++)
	{
		std::cout << mGBest[dimension] << std::endl;
	}

	std::cout << "The best fitness is: " << std::endl;
	std::cout << mPreviousGBestFitness;	

}

void PSOOptimizer::Initialize()
{
	// Initialize the particle positions and velocities
	// The positions and velocities are m x n vectors
	// m -> numberOfParticles
	// n -> numberOfDimensions
	std::vector<std::vector<double>> initialConditions(mNumberOfParticles, std::vector<double>(mDimensionsOfSearchSpace, 0));
	mParticlePositions = initialConditions;
	mParticleVelocities = initialConditions;
	mPBest = initialConditions;

	// For each particle and dimension generate random numbers to set as starting position and velocity
	for (int particleIdx = 0; particleIdx < mNumberOfParticles; particleIdx++)
	{
		int upperBound = mDimensionsOfSearchSpace;
//#pragma ivdep
#pragma omp simd
		for (int dimension = 0; dimension < upperBound; dimension++)
		{
			// Generate a random number between upper and lower bound for that dimension
			double randomPosition = (mUpperBounds[dimension] - mLowerBounds[dimension]) * ((double)rand() / (double)RAND_MAX) + mLowerBounds[dimension];
			double randomVelocity = (mUpperBounds[dimension] - mLowerBounds[dimension]) * ((double)rand() / (double)RAND_MAX) + mLowerBounds[dimension];

			// Set the values into the particle positions and velocities
			mParticlePositions[particleIdx][dimension] = randomPosition;
			mParticleVelocities[particleIdx][dimension] = randomVelocity;

			// Initial PBest is same as particle starting position
			mPBest[particleIdx][dimension] = randomPosition;

		}
		double particleFitness;
		std::vector<double> currentParticlePosition = mParticlePositions[particleIdx];
		int errCode = mObjectiveFunction(currentParticlePosition, particleFitness);
		mPreviousParticleBestFitness.push_back(particleFitness);
	}

	mGBest = mPBest[0];
	mPreviousGBestFitness = 9999999999999999.;
	//double* minElement = std::min_element(mPrevious)
	//mPreviousGBestFitness = ;
}

std::vector<double> PSOOptimizer::EvaluateParticleFitnesses()
{
	std::vector<double> currentParticleFitnesses;
#pragma omp simd
	for (int particleIdx = 0; particleIdx < mNumberOfParticles; particleIdx++)
	{
		double particleFitness;
		std::vector<double> currentParticlePosition = mParticlePositions[particleIdx];
		int errCode = mObjectiveFunction(currentParticlePosition, particleFitness);
		currentParticleFitnesses.push_back(particleFitness);
	}

	return currentParticleFitnesses;
}

void PSOOptimizer::UpdateParticleAndGlobalBest(std::vector<double> iCurrentParticleFitnesses)
{
	// Evaluate each particle against its previous fitness
		// If its fitness has improved (decreased in this case) update the corresponding
		// position vector of mPBest 2D vector


		// Once all the personal best positions of the particles have been evaluated,
		// find the particle with the best fitness. The position of that particle is mGBest
	for (int particleIdx = 0; particleIdx < mNumberOfParticles; particleIdx++)
	{
		if (iCurrentParticleFitnesses[particleIdx] < mPreviousParticleBestFitness[particleIdx])
		{
			mPreviousParticleBestFitness[particleIdx] = iCurrentParticleFitnesses[particleIdx];
			mPBest[particleIdx] = mParticlePositions[particleIdx];
			if (mPreviousGBestFitness > mPreviousParticleBestFitness[particleIdx])
			{
				mPreviousGBestFitness = mPreviousParticleBestFitness[particleIdx];
				mGBest = mPBest[particleIdx];
			}
			// else do nothing, we already have a best solution
		}
		// else do nothing
	}
}

void PSOOptimizer::UpdatePositionsAndVelocities()
{

	if (false == FLIP_LOOPS)
	{
		std::vector<double> r1(mDimensionsOfSearchSpace, 0);
		std::vector<double> cc1(mDimensionsOfSearchSpace, 0);
		std::vector<double> cc2(mDimensionsOfSearchSpace, 0);
		std::vector<double> r2(mDimensionsOfSearchSpace, 0);
		std::vector<double> w(mDimensionsOfSearchSpace, 0);
		int upperBound = mDimensionsOfSearchSpace;

		for (int dimension = 0; dimension < upperBound; dimension++)
		{
			cc1[dimension] = ((double)rand() / (double)RAND_MAX);
			cc2[dimension] = ((double)rand() / (double)RAND_MAX);
		}
		// For each particle and dimension, calculate the updated velocities and positions
		// The loops need to be inverted for efficient openmp parallelization. We are launching many threads which has significant overhead
		//omp_set_num_threads(4);
//#pragma omp parallel for	
		for (int particleIdx = 0; particleIdx < mNumberOfParticles; particleIdx++)
		{
			for (int dimension = 0; dimension < upperBound; dimension++)
			{
				r1[dimension] = ((double)rand() / (double)RAND_MAX);
				r2[dimension] = ((double)rand() / (double)RAND_MAX);
				w[dimension] = ((double)rand() / (double)RAND_MAX);

			}

			// This loop was vectorized but the speed up is only abour 1.8x. We should expect twice as much.
			// Probably need to make cache access more efficient.
#pragma omp simd
			for (int dimension = 0; dimension < upperBound; dimension++)
			{
				//Account for the self best influence in search
				//Generate random number between 0 and 1
				mParticleVelocities[particleIdx][dimension] += (w[dimension] - 1) * mParticleVelocities[particleIdx][dimension];
				mParticleVelocities[particleIdx][dimension] += r1[dimension] * cc1[dimension] * (mPBest[particleIdx][dimension] - mParticlePositions[particleIdx][dimension]);
				// Account for global best influence in search
				mParticleVelocities[particleIdx][dimension] += r2[dimension] * cc2[dimension] * (mGBest[dimension] - mParticlePositions[particleIdx][dimension]);
				// Update the new positions
				mParticlePositions[particleIdx][dimension] += mParticleVelocities[particleIdx][dimension];
			}
		}
	}
	else
	{
		std::vector<double> r1(mNumberOfParticles, 0);
		std::vector<double> cc1(mNumberOfParticles, 0);
		std::vector<double> cc2(mNumberOfParticles, 0);
		std::vector<double> r2(mNumberOfParticles, 0);
		std::vector<double> w(mNumberOfParticles, 0);
		int upperBound = mNumberOfParticles;

		for (int particleIdx = 0; particleIdx < upperBound; particleIdx++)
		{
			cc1[particleIdx] = ((double)rand() / (double)RAND_MAX);
			cc2[particleIdx] = ((double)rand() / (double)RAND_MAX);
		}
		//omp_set_num_threads(12);
#pragma omp parallel for 
		for (int dimension = 0; dimension < mDimensionsOfSearchSpace; dimension++)
		{
			for (int particleIdx = 0; particleIdx < upperBound; particleIdx++)
			{
				r1[particleIdx] = ((double)rand() / (double)RAND_MAX);
				r2[particleIdx] = ((double)rand() / (double)RAND_MAX);
				w[particleIdx] = ((double)rand() / (double)RAND_MAX);
			}
#pragma omp simd
			for (int particleIdx = 0; particleIdx < upperBound; particleIdx++)
			{
				//Account for the self best influence in search
				//Generate random number between 0 and 1
				mParticleVelocities[particleIdx][dimension] += (w[dimension] - 1) * mParticleVelocities[particleIdx][dimension];
				mParticleVelocities[particleIdx][dimension] += r1[dimension] * cc1[dimension] * (mPBest[particleIdx][dimension] - mParticlePositions[particleIdx][dimension]);
				// Account for global best influence in search
				mParticleVelocities[particleIdx][dimension] += r2[dimension] * cc2[dimension] * (mGBest[dimension] - mParticlePositions[particleIdx][dimension]);
				// Update the new positions
				mParticlePositions[particleIdx][dimension] += mParticleVelocities[particleIdx][dimension];
			}

		}
	}
	
}
