#include <iostream>
#include "PSOOptimizer.h"
#include "FunctionsToOptimize.h"
#include "kernel.h"
#include <chrono>

int main()
{
    // We need a function to optimize

    // We then need an optimizer object to set parameters on
    std::shared_ptr<PSOOptimizer> optimizer(new PSOOptimizer);

    optimizer->SetObjectiveFunction(FunctionsToOptimize::func1);

    optimizer->SetDefaultHyperParameters();
    
    // We need optimize function with hyper parameters
    optimizer->SetSearchDimensions(4);

    // Print results
    // I hate using "auto", but whatever. For now it works
    auto start = std::chrono::steady_clock::now();
    optimizer->Optimize();
    auto end = std::chrono::steady_clock::now();
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "Elapsed time in milliseconds : " << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << " mu s" << std::endl;


    getchar();

    return 0;

}
